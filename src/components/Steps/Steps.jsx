import { useState } from "react";
import LeftSide from "./components/LeftSide";
import StepOne from "./components/StepOne";
import { stepOneSchema } from "./components/StepOne/Schema";
import Styles from "./styles/Steps.module.css";

const Steps = () => {
  const [activeStep, setActiveStep] = useState(1);
  const [error, setError] = useState(null);

  const [stepOneInfo, setStepOneInfo] = useState({
    name: "",
    email: "",
    phoneNumber: "0",
  });

  const stepOneOnChange = (e) => {
    const inputName = e.target.name;
    setStepOneInfo({
      ...stepOneInfo,
      [inputName]: e.target.value,
    });
  };
  const stepOneOnSubmit = (e) => {
    e.preventDefault();
    const validation = Object.keys(stepOneSchema(stepOneInfo));

    if (validation.length > 0) {
      setError(stepOneSchema(stepOneInfo));
    } else {
      setActiveStep(2);
    }
  };
  return (
    <div className={Styles.steps}>
      <LeftSide activeStep={activeStep} />
      {activeStep === 1 && (
        <StepOne
          error={error}
          changeInputHandler={stepOneOnChange}
          onSubmitHandler={stepOneOnSubmit}
        />
      )}
    </div>
  );
};

export default Steps;
