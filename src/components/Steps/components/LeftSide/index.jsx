import React from "react";
import Styles from "./styles/LeftSide.module.css";
import backgroundImg from "./../../../../assets/images/bg-sidebar-desktop.svg";

const LeftSide = ({ activeStep }) => {
  return (
    <div className={Styles.wrapper}>
      <img src={backgroundImg} alt="main-bg" />
      <div className={Styles.content}>
        <div className={Styles.stepItem}>
          <div
            className={`${Styles.circle} ${activeStep === 1 && Styles.active}`}
          >
            1
          </div>
          <div className={Styles.itemInfo}>
            <div className={Styles.stepCount}>STEP 1</div>
            <div>YOUR INFO</div>
          </div>
        </div>
        <div className={Styles.stepItem}>
          <div
            className={`${Styles.circle} ${activeStep === 2 && Styles.active}`}
          >
            2
          </div>
          <div className={Styles.itemInfo}>
            <div className={Styles.stepCount}>STEP 2</div>
            <div>SELECT PLAN</div>
          </div>
        </div>
        <div className={Styles.stepItem}>
          <div
            className={`${Styles.circle} ${activeStep === 3 && Styles.active}`}
          >
            3
          </div>
          <div className={Styles.itemInfo}>
            <div className={Styles.stepCount}>STEP 3</div>
            <div>ADD-ONS</div>
          </div>
        </div>
        <div className={Styles.stepItem}>
          <div
            className={`${Styles.circle} ${activeStep === 4 && Styles.active}`}
          >
            4
          </div>
          <div className={Styles.itemInfo}>
            <div className={Styles.stepCount}>STEP 4</div>
            <div>SUMMARY</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LeftSide;
