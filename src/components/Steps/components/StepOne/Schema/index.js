const EMAIL_REGEX =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
const PHONE_NUMBER_REGEX =
    /^\+?([0-9]{2})\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

export const stepOneSchema = (stepOneInfo) => {
    const validation = {
        name: {
            error: true,
            message: "name is not validate",
        },
        email: {
            error: true,
            message: "email is not validate",
        },
        phoneNumber: {
            error: true,
            message: "phoneNumber is not validate",
        },
    };
    if (stepOneInfo.name.trim() !== "") {
        validation.name.error = false;
        validation.name.message = "";
    }
    if (EMAIL_REGEX.test(stepOneInfo.email)) {
        validation.email.error = false;
        validation.email.message = "";
    }

    if (PHONE_NUMBER_REGEX.test(stepOneInfo.phoneNumber)) {
        validation.phoneNumber.error = false;
        validation.phoneNumber.message = "";
    }

    const asArray = Object.entries(validation);
    const filtered = asArray.filter(([key, value]) => value.error);

    return Object.fromEntries(filtered);
};