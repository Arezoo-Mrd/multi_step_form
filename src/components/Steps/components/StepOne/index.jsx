import React from "react";
import Button from "../../../common/Button/styles";
import Input from "../../../common/Input";
import Styles from "./../../styles/Steps.module.css";

const StepOne = ({ changeInputHandler, onSubmitHandler, error }) => {
  const onChangeHandler = (e) => {
    changeInputHandler(e);
  };
  return (
    <form className={Styles.form} onSubmit={onSubmitHandler}>
      <h3>Personal info</h3>
      <p>Please provide your name, email address, and phone number.</p>
      <Input
        label={"Name"}
        name="name"
        error={error?.name}
        placeholder="e.g. Stephen King"
        onChange={(e) => onChangeHandler(e)}
      />
      <Input
        label={"Email Address"}
        error={error?.email}
        name="email"
        placeholder="e.g. stephenking@lorem.com"
        onChange={(e) => onChangeHandler(e)}
      />
      <Input
        label={"Phone Number"}
        name="phoneNumber"
        error={error?.phoneNumber}
        placeholder="e.g. +1 234 567 890"
        type="tel"
        onChange={(e) => onChangeHandler(e)}
      />
      <div className={Styles.submitBtn}>
        <Button />
      </div>
    </form>
  );
};

export default StepOne;
