import React from "react";
import Styles from "./Button.module.css";

const Button = () => {
  return (
    <button className={Styles.button} type="submit">
      Next Step
    </button>
  );
};

export default Button;
