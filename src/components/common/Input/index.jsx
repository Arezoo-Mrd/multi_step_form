import { useId } from "react";
import Styles from "./styles/Input.module.css";

const Input = ({
  type = "text",
  name,
  placeholder,
  label,
  onChange,
  error,
}) => {
  const uniqueId = useId();
  return (
    <div className={Styles.formGroup}>
      <label htmlFor={uniqueId}>{label} </label>
      <input
        id={uniqueId}
        className={`${error?.error ? Styles.error : ""}`}
        type={type}
        placeholder={placeholder}
        onChange={onChange}
        name={name}
      />
      <span className={Styles.errorMessage}>
        {error?.error ? error.message : ""}{" "}
      </span>
    </div>
  );
};

export default Input;
