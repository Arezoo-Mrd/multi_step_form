import React from "react";
import Styles from "./styles/Layout.module.css";

const Layout = ({ children }) => {
  return (
    <div className={Styles.layout}>
      <div className={Styles.wrapper}>{children}</div>
    </div>
  );
};

export default Layout;
