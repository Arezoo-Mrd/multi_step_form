import React from "react";
import "./App.css";
import Layout from "./components/Layout";
import Steps from "./components/Steps/Steps";

function App() {
  return (
    <Layout>
      <Steps />
    </Layout>
  );
}

export default App;
